//noinspection JSCheckFunctionSignatures
Ext.define('Extangled.BindModel',
{
	extend: 'Ext.data.Model',

	initializeScope: function(parentScope)
	{
		var me = this;

		if (!parentScope)
		{
			me.$scope = createBindScope();
		}
		else
		{
			me.$scope = parentScope.$new();
		}

		me.$scope.$record = me;

		me.fields.each(function(field)
		{
			me.$scope[field.name] = me.get(field.name) || null;
			me.$scope.$watch(field.name, function(newValue)
			{
				me._onScopeValueChange(field.name, newValue);
			});
		});
	},

	removeScope: function()
	{
		var me = this;

		if (!me.$scope)
		{
			return;
		}

		me.$scope.$record = null;
		me.$scope.$destroy();
		me.$scope = null;
	},

	_onScopeValueChange: function(fieldName, newValue)
	{
		var me = this;

		me._valuesChanging = true;
		me.set(fieldName, newValue);
		me.commit();
		console.log('scope change', fieldName, newValue);
		me._valuesChanging = false;
	},

	afterEdit : function(modifiedFieldNames)
	{
		var me = this;

		me.callParent(arguments);

		if (me._valuesChanging || !me.$scope)
		{
			return;
		}

		for (var i = 0; i < modifiedFieldNames.length; i++)
		{
			var fieldName = modifiedFieldNames[i];
			var value = me.get(fieldName);

			console.log('model change', fieldName, value);
			me.$scope[fieldName] = value;
		}

		me.$scope.$apply();
 	},

	$scope: null,
	_valuesChanging: false
});

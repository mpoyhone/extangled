//noinspection JSCheckFunctionSignatures
Ext.define('Test.BindStore',
{
	extend: 'Ext.data.Store',

	model: 'Test.BindModel',

	initializeScope: function(parentScope)
	{
		var me = this;

		if (me.$scope)
		{
			Ext.Error.raise('Scope is already initialized for grid.');
		}

		if (!parentScope)
		{
			me.$scope = createBindScope();
		}
		else
		{
			me.$scope = parentScope.$new();
		}

		me.$scope.$grid = me;
	},

	removeScope: function()
	{

		var me = this;

		if (!me.$scope)
		{
			return;
		}

		me.$scope.grid = null;
		me.$scope.$destroy();
		me.$scope = null;
	},

	/**
	 * Constructor for this class.
	 */
	constructor : function (config)
	{
		var me = this;

		me.callParent(arguments);

		me.on('add', me._onRecordsAdded, me);
		me.on('bulkremove', me._onRecordsRemoved, me);
	},

	_onRecordsAdded: function(store, records)
	{
		var me = this;

		if (!me.$scope)
		{
			Ext.Error.raise('No scope set for store.');
		}

		//noinspection JSCheckFunctionSignatures
		Ext.Array.each(records, function(record)
		{
			record.initializeScope(me.$scope);
		});
	},

	_onRecordsRemoved: function(store, records)
	{
		//noinspection JSCheckFunctionSignatures
		Ext.Array.each(records, function(record)
		{
			record.removeScope();
		});
	},

	$scope: null
});

function notifyScopeUpdate(scope)
{
	scope.$apply();
	//scope.$emit('ScopeUpdated', scope);
}

//noinspection JSCheckFunctionSignatures
Ext.define('Extangled.RootBinder',
{
	/**
	 * Binding registry to use.
	 * @type Extangled.binding.BindingRegistry
	 */
	registry: null,

	initialize: function(component, $injector)
	{
		var me = this;

		me._rootComponent = component;
		me._rootScope = createBindScope();

		me._initializeComponents(me._rootComponent, me._rootScope);
	},

	_initializeComponents: function(component, parentScope)
	{
		var me = this;

		if (!component)
		{
			return;
		}

		if (component.$controller)
		{
			var scope = parentScope.$new();

			window[component.$controller](scope);
			component.$scope = scope;
		}

		var componentScope = component.$scope || parentScope;

		if (component.items)
		{
			component.items.each(function(child)
			{
				me._initializeComponents(child, componentScope);
			});
		}

		if (component.isXType('grid'))
		{
			var gridHeader = component.getView().headerCt;
			me._initializeComponents(gridHeader, componentScope);
		}

		if (component.bind)
		{
			me._createBindings(component, componentScope);
		}
	},

	_createBindings: function(component, scope)
	{
		var me = this;

		//noinspection JSCheckFunctionSignatures
		Ext.Array.each(component.bind, function(binding)
		{
			var factory = me.registry.getBindingFactory(component, binding.property);

			if (!factory)
			{
				Ext.Error.raise('No binding factory found for component: ' + component.$className);
			}

			var bindingInstance = factory.createBinding(scope, component, binding);

			bindingInstance.initialize();
		});
	},

	/**
	 * Constructor for this class.
	 */
	constructor : function (config)
	{
		var me = this;

		Ext.apply(me, config);
		me.callParent(arguments);

		if (!me.registry)
		{
			Ext.Error.raise('Binding registry is not specified.');
		}
	},

	_rootComponent: null,
	_rootScope: null
});

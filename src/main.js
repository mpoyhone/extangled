
var mainFn = function()
{
    //noinspection JSCheckFunctionSignatures
	Ext.define('Book',{
        extend: 'Extangled.BindModel',

		idProperty: 'id',
        fields: [
			{ name: 'id', type: 'int' },
            'firstName',
            'lastName',
			{ name: 'age', type: 'int' }
        ]
    });

    // create the Data Store
    var store = Ext.create('Ext.data.JsonStore', {
        model: 'Book',
        data:
		[
			{ id: 1, firstName: 'Donald', lastName: 'D', age: 33 },
			{ id: 2, firstName: 'Mickey', lastName: 'M', age: 24 },
			{ id: 3, firstName: 'Goofy', lastName: '?', age: 22 },
			{ id: 4, firstName: 'Pluto', lastName: 'D', age: 4 }
		]
    });

    var panel = Ext.create('Ext.Panel', {
		$controller: 'myController',
        renderTo: 'binding-example',
        frame: true,
        title: 'Book List',
        width: 580,
        height: 400,
        layout: 'border',
        items: [
			{
				xtype: 'grid',
				itemId: 'myGrid',
				bind:
				[
					{
						property : 'selection',
						expr	 : 'user'
					}
				],
				store: store,
				columns: [
					{
						width: 120, dataIndex: 'firstName', editor: 'textfield', sortable: true,
						bind:
						[
							{
								property: 'label',
								expr	: 'user.firstName'
							}
						]
					},
					{
						flex: 1, dataIndex: 'lastName', editor: 'textfield', sortable: true,
						bind:
						[
							{
								property: 'label',
								expr	: 'user.lastName'
							}
						]
					},
					{
						width: 125, dataIndex: 'age', editor: 'numberfield', sortable: true,
						bind:
						[
							{
								property: 'label',
								expr	: 'user.age'
							}
						]
					}
				],
				plugins: [
					Ext.create('Ext.grid.plugin.CellEditing', {
						clicksToEdit: 1
					})
				],
				height:210,
				split: true,
				region: 'north'
			},
			{
				xtype: 'form',
                id: 'detailPanel',
                itemId: 'detailPanel',
                region: 'center',
                bodyPadding: 7,
				items:
				[
					{
						xtype: 'textfield',
						fieldLabel: 'First name',
						bind:
						[
							{
								property: 'label',
								expr	: 'user.firstName'
							},
							{
								property: 'value',
								expr	: 'user.firstName'
							},
							{
								property: 'focused',
								expr	: 'firstNameFocused'
							}
						]
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Last name',
						bind:
						[
							{
								property: 'label',
								expr	: 'user.lastName'
							},
							{
								property: 'value',
								expr	: 'user.lastName'
							},
							{
								property: 'visible',
								expr	: '!firstNameFocused'
							}
						]
					},
					{
						xtype: 'displayfield',
						fieldLabel: 'Full name',
						bind:
						[
							{
								property: 'value',
								expr	: 'fullName(user)',
								bindFrom: false
							},
							{
								property: 'label',
								expr	: 'user.fullName'
							}
						]
					},
					{
						xtype: 'numberfield',
						fieldLabel: 'Age',
						bind:
						[
							{
								property: 'value',
								expr	: 'user.age'
							},
							{
								property: 'enabled',
								expr	: 'isNameSet(user)'
							},
							{
								property: 'label',
								expr	: 'user.age'
							}
						]
					},
					{
						xtype: 'button',
						text: 'Click',
						bind:
						[
							{
								property: 'label',
								expr	: 'buttons.Ok'
							},
							{
								property: 'click',
								expr	: 'onButtonClicked()'
							},
							{
								property: 'enabled',
								expr	: '!firstNameFocused'
							}
						]
					}
				]
        }]
    });

	// Controller.
	window.myController = function($scope)
	{
		$scope.firstNameFocused = false;
		$scope.labels =
		{
			buttons:
			{
				Ok		: 'Ok',
				Cancel	: 'Cancel'
			},
			user:
			{
				firstName: 'First name',
				lastName: 'Last name',
				fullName: 'Full name',
				age: 'Age'
			}

		};

		$scope.isNameSet = function(user)
		{
			return !!user.firstName && !!user.lastName;
		};

		$scope.fullName = function(user)
		{
			return user.firstName + ' ' +  user.lastName;
		};

		$scope.onButtonClicked = function()
		{
			console.log('Clicked');
			$scope.labels.buttons.Ok += '!';
			$scope.$apply();

			var record = store.last();

			record.set('age', parseInt(Math.random() * 100));
			record.commit();
		};
	};

	var registry = Extangled.binding.BindingConfig.getRegistry();
	var rootBinder = new Extangled.RootBinder({ registry: registry });

	rootBinder.initialize(panel, null);
};

Ext.require(
	[
		'Extangled.BindModel',
		'Extangled.binding.BindingConfig',
		'Extangled.RootBinder'
	],
	function()
	{
		Ext.onReady(mainFn);
	}
);

//noinspection JSCheckFunctionSignatures
Ext.define('Extangled.binding.AbstractLabelBinding',
{
	extend: 'Extangled.binding.SimpleBinding',

	bindFrom 			: false,
	updateOnInitialize	: true,

	/**
	 * Creates a binding from AngularJS scope expression to ExtJS component.
	 */
	_bindToExt: function()
	{
		var me = this;

		me.callParent(arguments);

		if (!me.component.rendered)
		{
			me.component.on('afterrender', me._onAfterRender, me, { single : true });
		}
		else
		{
			me._onAfterRender();
		}
	},

	/**
	 * Called after the component has been rendered.
	 * @private
	 */
	_onAfterRender: function()
	{
		var me = this;
		var labelEl = me._getLabelEl();
		var highlightCls = 'translation-highlight';

		if (labelEl)
		{
			labelEl.on('click', me._openLabelClick, me);
			labelEl.hover(
				function(e)
				{
					if (e.ctrlKey && e.shiftKey)
					{
						labelEl.addCls(highlightCls);
					}
				},
				function()
				{
					labelEl.removeCls(highlightCls);
				});
		}
	},

	/**
	 * Returns the label Ext.Element from the current component.
	 * @private
	 */
	_getLabelEl: function()
	{
		return null;
	},

	/**
	 * Called when component label is clicked this opens the label edit window if
	 * control and shift keys is down.
	 * @private
	 */
	_openLabelClick: function(e)
	{
		var me = this;

		if (e.ctrlKey && e.shiftKey)
		{
			e.stopEvent();

			if (!me._scopeAssignFn)
			{
				me._createAssignmentFn(me.expr);
			}

			var currentValue = me._readScopeValue();
			var fn = function(button, value)
			{
				if (button == 'ok')
				{
					me._scopeAssignFn(me.scope, value);
					me.scope.$apply();
				}
			};

			Ext.Msg.prompt('Enter label', 'Enter new value for ' + me.expr, fn, me, true, currentValue);
		}
	},

	/**
	 * Constructor for this class.
	 */
	constructor : function (config)
	{
		var me = this;

		config.expr = 'labels.' + config.expr;
		me.callParent(arguments);
	}
});

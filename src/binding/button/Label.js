//noinspection JSCheckFunctionSignatures
Ext.define('Extangled.binding.button.Label',
{
	extend: 'Extangled.binding.AbstractLabelBinding',

	setter: 'setText',

	/**
	 * Returns the label Ext.Element from the current component.
	 * @private
	 */
	_getLabelEl: function()
	{
		return this.component.getEl();
	}
});

//noinspection JSCheckFunctionSignatures
Ext.define('Extangled.binding.BindingRegistry',
{
	requires:
	[
		'Extangled.binding.BindingFactory'
	],

	/**
	 * Parent registry instance.
	 */
	parent: null,

	/**
	 * Registers a binding factory by component xtype.

	 * @param {string} xtype Component XType
	 * @param {string} property Component property
	 * @param {Extangled.binding.BindingFactory} factory Binding factory.
	 */
	registerWithXType: function(xtype, property, factory)
	{
		var me = this;
		var key = me._getXTypeMapKey(xtype, property);

		me._xtypeMap[key] = factory;
	},

	/**
	 * Registers a binding factory by Javascript property (e.g. isFieldLabelable).

	 * @param {string} jsPropertyName Component Javascript property.
	 * @param {object} jsPropertyValue Component Javascript value.
	 * @param {string} property Component property
	 * @param {Extangled.binding.BindingFactory} factory Binding factory.
	 */
	registerWithJsProperty: function(jsPropertyName, jsPropertyValue, property, factory)
	{
		var me = this;

		me._jsPropertyList.push(
		{
			jsPropertyName	: jsPropertyName,
			jsPropertyValue	: jsPropertyValue,
			property		: property,
			factory			: factory
		});
	},

	/**
	 * Returns a binding configuration for the given component.
	 * @param {Ext.Component} component Component.
	 * @param {string} property Binding property name.
	 */
	getBindingFactory: function(component, property)
	{
		var me = this;
		var factory = null;
		var i, count;

		for (i = 0, count = me._jsPropertyList.length; i < count; i++)
		{
			var jsPropEntry = me._jsPropertyList[i];

			if (jsPropEntry.property == property && component[jsPropEntry.jsPropertyName] === jsPropEntry.jsPropertyValue)
			{
				factory = jsPropEntry.factory;
				break;
			}
		}

		if (!factory)
		{
			var xtypes = Ext.Array.unique(component.getXTypes().split('/'));
			var key;

			for (i = xtypes.length - 1; i >= 0; i--)
			{
				key = me._getXTypeMapKey(xtypes[i], property);

				if (me._xtypeMap.hasOwnProperty(key))
				{
					factory = me._xtypeMap[key];
					break;
				}
			}
		}

		if (!factory && me.parent)
		{
			factory = me.parent.getBindingFactory(component, property);
		}

		return factory ? factory : me._defaultBindingFactory;
	},

	/**
	 * Returns key for the binding configuration map.
	 * @param {string} xtype Component XType
	 * @param {string} property Component property
	 * @returns {string}
	 * @private
	 */
	_getXTypeMapKey: function(xtype, property)
	{
		return xtype + '|' + property;
	},

	/**
	 * Constructor for this class.
	 */
	constructor : function (config)
	{
		var me = this;

		Ext.apply(me, config);
		me.callParent(arguments);

		me._xtypeMap = {};
		me._jsPropertyList = [];
	},

	/**
	 * Maps from 'xtype|propertyname' to the binding factory.
	 */
	_xtypeMap: null,
	/**
	 * Array containing mappings by component Javascript property.
	 */
	_jsPropertyList: null,
	/**
	 * Binding factory to use when no specific rules are found.
	 */
	_defaultBindingFactory: null
});

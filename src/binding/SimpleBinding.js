//noinspection JSCheckFunctionSignatures
Ext.define('Extangled.binding.SimpleBinding',
{
	extend: 'Extangled.binding.AbstractBinding',

	/**
	 * Function to set value to ExtJS component. Can also be setter function name.
	 * @type Function
	 */
	setter: null,
	/**
	 * Name of ExtJS event to listen.
	 * @type String
	 */
	eventName: null,
	/**
	 * Optional filter function for converting event parameters to the binding value.
	 * Function is called when the same arguments as the event listener.
	 * Used only of 'eventName' is set.
	 * @type Function
	 */
	eventFilterFn: null,

	/**
	 * Sets ExtJS component value. This version does not check the flags.
	 * @param value
	 * @protected
	 */
	_setExtValue: function(value)
	{
		var me = this;

		me._extSetter.apply(me.component, [ value ]);
	},

	/**
	 * Sets a value in the scope based on the expression.
	 * This version does not check the flags.
	 * @param value Value to set.
	 * @protected
	 */
	_setScopeValue: function(value)
	{
		var me = this;

		me._scopeAssignFn(me.scope, value);
		me.scope.$apply();
	},

	/**
	 * Creates a binding from AngularJS scope expression to ExtJS component.
	 */
	_bindToExt: function()
	{
		var me = this;
		var setter = Ext.isString(me.setter) ? me.component[me.setter] : me.setter;

		if (!Ext.isFunction(setter))
		{
			Ext.Error.raise('Setter not found: ' + me.component.$className + ':' + me.setter);
		}

		me._extSetter = setter;
		me.callParent(arguments);
	},

	/**
	 * Constructor for this class.
	 */
	constructor : function(config)
	{
		var me = this;

		me.callParent(arguments);

		if (me.property && !me.setter)
		{
			var capitalName = Ext.String.capitalize(me.property.toLowerCase());

			me.setter = 'set' + capitalName;
		}

		if (me.eventName && !me.eventConfig)
		{
			var filterFn = me.eventFilterFn;

			me.eventConfig = {};
			me.eventConfig[me.eventName] = function(c, value)
			{
				var eventValue = value;

				if (filterFn)
				{
					eventValue = filterFn.apply(me, arguments);
				}

				me.setScopeValue(eventValue);
			};
		}
	},

	/**
	 * Function used to set the value in ExtJS component.
	 */
	_extSetter: null
});

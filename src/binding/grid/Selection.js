//noinspection JSCheckFunctionSignatures
Ext.define('Extangled.binding.grid.Selection',
{
	extend: 'Extangled.binding.AbstractBinding',

	eventConfig:
	{
		selectionchange : function(sm, selection)
		{
			var me = this;
			var value;

			value = Ext.Array.map(selection || [], function(r)
			{
				if (!r.$scope)
				{
					r.initializeScope(me.scope);
				}

				return r.$scope;
			});

			if (me.component.getSelectionModel().getSelectionMode() != 'MULTI')
			{
				value = value.length > 0 ? value[0] : null;
			}

			me.setScopeValue(value);
		}
	},

	/**
	 * Sets ExtJS grid selection.
	 * @param value Either a single record or array of records.
	 */
	_setExtValue: function(value)
	{
		var me = this;
		var selection = Ext.isArray(value) ? value : (value ? [ value ] : []);

		me.component.getSelectionModel().select(selection);
	},

	/**
	 * Sets a value in the scope based on the expression.
	 * This version does not check the flags.
	 * @param value Value to set.
	 */
	_setScopeValue: function(value)
	{
		var me = this;

		me._scopeAssignFn(me.scope, value);
		me.scope.$apply();
	}
});

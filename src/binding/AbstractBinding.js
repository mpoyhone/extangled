//noinspection JSCheckFunctionSignatures
Ext.define('Extangled.binding.AbstractBinding',
{
	/**
	 * AngularJS scope.
	 */
	scope: null,
	/**
	 * ExtJS component.
	 */
	component: null,
	/**
	 * AngularJS expression to bind to.
	 * @type String
	 */
	expr: null,
	/**
	 * Function to set value to ExtJS component. Can also be setter function name.
	 * @type Function
	 */
	setter: null,
	/**
	 * ExtJS event configuration object. Can contain multiple listeners. The
	 * scope will be set to this binding object.
	 * @type Object
	 */
	eventConfig: null,
	/**
	 * true to bind to ExtJS component.
	 * @type boolean
	 */
	bindTo: true,
	/**
	 * true to bind from ExtJS component.
	 * @type boolean
	 */
	bindFrom: true,
	/**
	 * Update ExtJS component value when initialized.
	 */
	updateOnInitialize: false,

	/**
	 * Sets ExtJS component value.
	 * @param value
	 */
	setExtValue: function(value)
	{
		var me = this;

		if (me._isFiring || me._isDisabled)
		{
			return;
		}

		try
		{
			me._isFiring = true;
			me._setExtValue(value);
		}
		finally
		{
			me._isFiring = false;
		}
	},

	/**
	 * Sets a value in the scope based on the expression.
	 * @param value Value to set.
	 */
	setScopeValue: function(value)
	{
		var me = this;

		if (!me._scopeAssignFn)
		{
			Ext.Error.raise('Scope assignment function is not set.');
		}

		if (me._isFiring || me._isDisabled)
		{
			return;
		}

		try
		{
			me._isFiring = true;
			me._setScopeValue(value);
		}
		finally
		{
			me._isFiring = false;
		}
	},

	/**
	 * Initializes the binding.
	 */
	initialize: function()
	{
		var me = this;

		if (me.bindTo)
		{
			me._bindToExt();
		}

		if (me.bindFrom)
		{
			me._bindFromExt();
		}

		if (me.updateOnInitialize)
		{
			me.setExtValue(me._readScopeValue());
		}
	},

	/**
	 * Sets ExtJS component value. This version does not check the flags.
	 * Subclasses must implement this.
	 * @param value
	 * @protected
	 */
	_setExtValue: function(value)
	{
	},

	/**
	 * Sets a value in the scope based on the expression.
	 * This version does not check the flags.
	 * Subclasses must implement this.
	 * @param value Value to set.
	 * @protected
	 */
	_setScopeValue: function(value)
	{
	},

	/**
	 * Creates a binding from AngularJS scope expression to ExtJS component.
	 */
	_bindToExt: function()
	{
		var me = this;

		me.scope.$watch(me.expr, function(newValue)
		{
			me.setExtValue(newValue);
		});
	},

	/**
	 * Creates a binding from ExtJS component to AngularJS scope.
	 */
	_bindFromExt: function()
	{
		var me = this;

		if (!me.eventConfig)
		{
			Ext.Error.raise('Event configuration is not set.');
		}

		me._createAssignmentFn(me.expr);
		me.component.on(me.eventConfig);
	},

	/**
	 * Creates a function which assigns a value in the AngularJS scope.
	 * @param {String} exprText AngularJS expression.
	 * @returns {Function} Function is called with (scope, value) arguments.
	 */
	_createAssignmentFn: function(exprText)
	{
		var me = this;
		var expr;

		angular.injector(['ng']).invoke(function($interpolate)
		{
			expr = $interpolate('{{' + exprText + ' = __tmp_value__}}');
		});

		me._scopeAssignFn = function(scope, value)
		{
			scope.__tmp_value__ = value;
			expr(scope);
			delete scope.__tmp_value__;
		};
	},

	/**
	 * Reads the value from the scope.
	 * @returns Scope value.
	 */
	_readScopeValue: function()
	{
		var me = this;
		var value;

		angular.injector(['ng']).invoke(function($interpolate)
		{
			value = $interpolate('{{' + me.expr + '}}')(me.scope);
		});

		return value;
	},

	/**
	 * Constructor for this class.
	 */
	constructor : function(config)
	{
		var me = this;

		Ext.apply(me, config);
		me.callParent(arguments);

		if (me.eventConfig)
		{
			me.eventConfig.scope = me;
		}
	},

	/**
	 * true if this binding is already firing. Used to disable event firing to the other direction.
	 */
	_isFiring: false,
	/**
	 * true if this binding has been disabled.
 	 */
	_isDisabled: false,
	/**
	 * Function used to assign a value a scope. This is created using the 'expr' scope expression.
	 */
	_scopeAssignFn: null
});

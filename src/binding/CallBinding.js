//noinspection JSCheckFunctionSignatures
Ext.define('Extangled.binding.CallBinding',
{
	extend: 'Extangled.binding.SimpleBinding',

	bindTo: false,

	/**
	 * Creates a function which assigns a value in the AngularJS scope.
	 * @param {String} exprText AngularJS expression.
	 * @returns {Function} Function is called with (scope, value) arguments.
	 */
	_createAssignmentFn: function(exprText)
	{
		var me = this;
		var expr;

		angular.injector(['ng']).invoke(function($interpolate)
		{
			expr = $interpolate('{{' + exprText + '}}');
		});

		me._scopeAssignFn = function(scope)
		{
			expr(scope);
		};
	},

	/**
	 * Constructor for this class.
	 */
	constructor : function(config)
	{
		var me = this;

		if (!config.eventName)
		{
			config.eventName = config.property;
		}

		me.callParent(arguments);
	}
});

//noinspection JSCheckFunctionSignatures
Ext.define('Extangled.binding.BindingFactory',
{
	requires:
	[
		'Extangled.binding.SimpleBinding'
	],

	/**
	 * Name of class used for the actual binding.
	 * @type String
	 */
	bindingClass: 'Extangled.binding.SimpleBinding',
	/**
	 * Configuration for the Binder class.
	 */
	config: null,

	/**
	 * Creates a binding from the scope using the given expression
	 * to the component using the bindingConfig (contains the property name);
	 * @param {Object} scope AngularJS scope.
	 * @param {Ext.Component} component ExtJS component to bind to.
	 * @param {Object} bindingConfig Binding configuration from the 'bind' property.
	 * @returns {Extangled.binding.Binding} Binding instance. Is has not been initialized yet.
	 */
	createBinding: function(scope, component, bindingConfig)
	{
		var me = this;
		var cfg = Ext.apply({}, bindingConfig || {}, me.config || {});

		cfg.scope = scope;
		cfg.component = component;
		me._validateBindingConfig(cfg);

		return Ext.create(me.bindingClass, cfg);
	},

	/**
	 * Validates the binding configuration object before it is passed to the binding.
	 * @param cfg
	 * @private
	 */
	_validateBindingConfig: function(cfg)
	{
		if (!cfg.expr)
		{
			Ext.Error.raise('Missing scope expression.');
		}

		if (!cfg.component)
		{
			Ext.Error.raise('Missing component.');
		}

		if (!cfg.scope)
		{
			Ext.Error.raise('Missing scope.');
		}
	},

	/**
	 * Constructor for this class.
	 */
	constructor : function (config)
	{
		var me = this;

		Ext.apply(me, config);
		me.callParent(arguments);
	}
});

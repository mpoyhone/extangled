/**
 * Registers default binding configuration to a binding registry.
 */
//noinspection JSCheckFunctionSignatures
Ext.define('Extangled.binding.BindingConfig',
{
	requires:
	[
		'Extangled.binding.BindingRegistry',
		'Extangled.binding.BindingFactory',
		'Extangled.binding.CallBinding',
		'Extangled.binding.grid.Selection',
		'Extangled.binding.grid.column.Label',
		'Extangled.binding.field.Label',
		'Extangled.binding.button.Label'
	],

	singleton: true,

	/**
	 * Returns the global registry.
	 */
	getRegistry: function()
	{
		var me = this;

		if (!me._defaultBindingRegistry)
		{
			me._defaultBindingRegistry = new Extangled.binding.BindingRegistry();
			me.registerBindings(me._defaultBindingRegistry);
		}

		return me._defaultBindingRegistry;
	},

	/**
	 * Registers bindings to the registry.
	 */
	registerBindings: function(registry)
	{
		var register = function(componentXType, propertyName, config)
		{
			registry.registerWithXType(componentXType, propertyName, new Extangled.binding.BindingFactory(
			{
				config: Ext.apply(
				{
					property: propertyName
				}, config)
			}));
		};

		register('component', 'active',
		{
			eventConfig:
			{
				activate	: function() { this.setScopeValue(true); },
				deactivate	: function() { this.setScopeValue(false); }
			},
			bindFrom: false
		});
		register('component', 'enabled',
		{
			setter: function(value)
			{
				this.setDisabled(!value);
			},
			eventConfig:
			{
				enable	: function() { this.setScopeValue(true); },
				disable	: function() { this.setScopeValue(false); }
			},
			bindFrom: false
		});
		register('component', 'focused',
		{
			setter: function(value)
			{
				if (value)
				{
					this.focus();
				}
			},
			eventConfig:
			{
				focus	: function() { this.setScopeValue(true); },
				blur	: function() { this.setScopeValue(false); }
			}
		});
		register('component', 'visible',
		{
			setter: function(value)
			{
				this.setVisible(value);
			},
			eventConfig:
			{
				show	: function() { this.setScopeValue(true); },
				hide	: function() { this.setScopeValue(false); }
			},
			bindFrom: false
		});

		register('field', 'value', { eventName: 'change' });

		registry.registerWithXType('button', 'click', new Extangled.binding.BindingFactory(
		{
			bindingClass: 'Extangled.binding.CallBinding'
		}));
		registry.registerWithXType('button', 'label', new Extangled.binding.BindingFactory(
		{
			bindingClass: 'Extangled.binding.button.Label'
		}));

		registry.registerWithXType('grid', 'selection', new Extangled.binding.BindingFactory(
		{
			bindingClass: 'Extangled.binding.grid.Selection'
		}));
		registry.registerWithXType('gridcolumn', 'label', new Extangled.binding.BindingFactory(
		{
			bindingClass: 'Extangled.binding.grid.column.Label'
		}));

		registry.registerWithJsProperty('isFieldLabelable', true, 'label', new Extangled.binding.BindingFactory(
		{
			bindingClass: 'Extangled.binding.field.Label'
		}));
	},

	/**
	 * Global registry.
	 */
	_defaultBindingRegistry: null
});

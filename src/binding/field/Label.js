//noinspection JSCheckFunctionSignatures
Ext.define('Extangled.binding.field.Label',
{
	extend: 'Extangled.binding.AbstractLabelBinding',

	setter: 'setFieldLabel',

	/**
	 * Returns the label Ext.Element from the current component.
	 * @private
	 */
	_getLabelEl: function()
	{
		return this.component.labelEl;
	}
});
